直接下载解压后，把aardio-syntax-highlight目录拷贝到C:\Users\admin\AppData\Roaming\Sublime Text 3\Packages\User\目录下即可。

只是提供一种编辑器解决方案，因为时间有限，没有完全把aardio语法移植过来，大家可以更新完善。

![代码着色](https://gitee.com/uploads/images/2018/0506/152449_25934063_1919093.png "st3.png")
![自动补全](https://gitee.com/uploads/images/2018/0506/152615_9d047323_1919093.png "ST2.png")